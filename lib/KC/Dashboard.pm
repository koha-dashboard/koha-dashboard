package KC::Dashboard;

# Script to create dashboard.koha-community.org
# Copyright chris@bigballofwax.co.nz 2012,2013

our $VERSION = '0.1';

use Dancer;
use Dancer::Plugin::Database;
use DateTime;

use Modern::Perl;
use KC::Data ':all';
use DBI;
use Template;
use Text::CSV;

my $server_url = 'https://dashboard.koha-community.org';


set 'session' => 'Simple';

set 'show_errors'  => 1;
set 'startup_info' => 1;
set 'warnings'     => 1;

get '/' => sub {
    my $bugs_dbh = database('bugs');

    if ( $DEBUG ) {
        my $max_ts = $bugs_dbh->selectrow_array(q|SELECT DATE(MAX(creation_ts)) FROM bugs|);
        ( $cur_year, $cur_month, $cur_day ) = split ('-', $max_ts);
        $now = DateTime->new( year => $cur_year, month => $cur_month, day => $cur_day );
        $server_url = 'http://localhost:3000';
    }
    else {
        $now = DateTime->now;
    }

    set_now($now);

    my $entries  = last5signoffs($bugs_dbh);
    my $stats    = monthlyactivity( $bugs_dbh, 'Signed Off' );
    my $qa       = monthlyactivity( $bugs_dbh, 'Passed QA' );
    my $failedqa = monthlyactivity( $bugs_dbh, 'Failed QA' );
    my $yearsign = yearlyactivity( $bugs_dbh, 'Signed Off' );
    my $yearpass = yearlyactivity( $bugs_dbh, 'Passed QA' );
    my $yearfail = yearlyactivity( $bugs_dbh, 'Failed QA' );
    my $rescued  = monthlyrescues( $bugs_dbh );
    my $yearresc = yearlyrescues( $bugs_dbh );
    my $documented = monthlydocs( $bugs_dbh );
    my $yeardocs = yearlydocs( $bugs_dbh );

    my $sql = "SELECT count(*) as count, DATE(bug_when) as day FROM bugs_activity WHERE date(bug_when) = ? GROUP BY DATE(bug_when);";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($now->clone->subtract(days => 1)->ymd('/')) or die $sth->errstr;
    my $yesterday = $sth->fetchrow_hashref();
    
    $sql =
"SELECT count(*) as count, DATE(bug_when) as day FROM bugs_activity WHERE date(bug_when) = ? GROUP BY DATE(bug_when);";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($now->ymd('/')) or die $sth->errstr;
    my $today = $sth->fetchrow_hashref();
    
    $sql =
"SELECT count(*) as count, DATE(bug_when) as day FROM bugs_activity WHERE date(bug_when) = ? GROUP BY DATE(bug_when);";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($now->clone->subtract(days => 2)->ymd('/')) or die $sth->errstr;
    my $daybefore = $sth->fetchrow_hashref();
    
    $sql =
"SELECT bugs.bug_id,short_desc,bug_when FROM bugs,bugs_activity WHERE bugs.bug_id = bugs_activity.bug_id AND bugs.product_id = 2
AND added = 'Pushed to Master' AND (bug_severity = 'enhancement' OR bug_severity ='new feature') ORDER BY bug_when desc LIMIT 10";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    my $enhancement = $sth->fetchall_arrayref;
    my $dates       = get_dates();

    $sql =
"SELECT b.bug_id, short_desc, MAX(bug_when) as bug_when FROM bugs b, bugs_activity ba WHERE b.bug_id=ba.bug_id AND b.product_id = 2 AND b.bug_status='Signed Off' AND ba.added='Signed Off' GROUP BY b.bug_id ORDER BY bug_when LIMIT 10";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    my $old_nqa = $sth->fetchall_arrayref;
#    my $devs = get_last_devs();

    my $packages = get_package_versions();

    template 'show_entries.tt', {
        'server_url'  => $server_url,
        'entries'     => $entries,
        'stats'       => $stats,
        'yesterday'   => $yesterday,
        'today'       => $today,
        'daybefore'   => $daybefore,
        'enhancments' => $enhancement,
        'dates'       => $dates,
        'qa'          => $qa,
        'failed'      => $failedqa,
        'old_nqa'     => $old_nqa,
        'rescued'     => $rescued,
        'documented'  => $documented,
        'yearsign'    => $yearsign,
        'yearpass'    => $yearpass,
        'yearfail'    => $yearfail,
        'yearresc'    => $yearresc,
        'yeardocs'    => $yeardocs,
        'year'        => $cur_year,
        'month_name'  => DateTime->now->month_name,
        (
            $DEBUG
            ? (
                debug_mode => $DEBUG,
                cur_year   => $cur_year,
                cur_month  => $cur_month,
                cur_day    => $cur_day,
              )
            : ()
          ),
#        'devs'        => $devs,
        #        'ohloh'       => $ohloh,
        packages => $packages,
        supported_versions => get_supported_versions(),
    };
};

get '/bug_status' => sub {
    my $bugs_dbh = database('bugs');
    my $sql =
      "SELECT count(*) as count,bug_status FROM bugs GROUP BY bug_status";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    $sql =
"SELECT count(*) as count,bug_status FROM bugs WHERE bug_severity <> 'enhancement'
    AND bug_severity <> 'new feature' GROUP BY bug_status";
    my $sth2 = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth2->execute;
    my $all= $sth->fetchall_hashref('bug_status');
    my $bugs = $sth2->fetchall_hashref('bug_status');
    my @statuses = (
        "Needs documenting",
        "Needs Signoff",
        "Signed Off",
        "Passed QA",
        "Pushed to Master",
        "Failed QA",
        "Patch doesn't apply",
        "In Discussion",
    );
    my ($status, $bugssign);
    for my $s (@statuses) {
        $status->{$s} = $all->{$s}{count} || 0;
        $bugssign->{$s} = $bugs->{$s}{count} || 0;
    };
    template 'bug_status.tt',
      {
        'status'   => $status,
        'bugssign' => $bugssign,
      };
};

get '/bz_status' => sub {
    my $hide_details  = param 'hide_details';
    my $bugs_dbh      = database('bugs');
    my $health_status = health_status($bugs_dbh);
    template 'bz_status.tt',
      { health_status => $health_status, hide_details => $hide_details, };
};

get '/pushed_by_day' => sub {
    my $bugs_dbh      = database('bugs');
    my $pushedD  = pushed_by_day( $bugs_dbh );
    content_type 'application/json';
    return to_json $pushedD;
};

get '/randombug' => sub {
    my $sql =
"SELECT * FROM (SELECT bug_id,short_desc FROM bugs WHERE bug_status NOT in 
    ('CLOSED','RESOLVED','Pushed to master','Pushed to stable','Pushed to oldstable','Pushed to oldoldstable','VERIFIED', 'Signed Off', 'Passed QA') ) AS bugs2 ORDER BY rand() LIMIT 1";
    my $sth = database('bugs')->prepare($sql) or die database('bugs')->errstr;
    $sth->execute or die $sth->errstr;

    template 'randombug.tt', { 'randombug' => $sth->fetchall_arrayref };
};

get '/randomquote' => sub {
    open FILE, 'data/koha_irc_quotes.txt' || die "can't open file";
    my @quotes    = <FILE>;
    my $quote     = $quotes[ rand @quotes ];
    my $csv       = Text::CSV->new( { binary => 1 } );
    my $linequote = $csv->parse($quote);
    template 'quote.tt', { 'quote' => $csv };
};

get '/rq' => sub {
    open FILE, 'data/koha_irc_quotes.txt' || die "can't open file";
    my @quotes    = <FILE>;
    my $quote     = $quotes[ rand @quotes ];
    my $csv       = Text::CSV->new( { binary => 1 } );
    my $linequote = $csv->parse($quote);
    template 'quotetext.tt', { 'quote' => $csv };
};

get '/needsignoff' => sub {
    my $bugs_dbh = database('bugs');
    my $sql      = "SELECT bugs.bug_id,short_desc FROM bugs
               WHERE bug_status ='Needs Signoff' and bug_severity <> 'enhancement' and product_id = 2 and
               bug_severity <> 'new feature' ORDER by lastdiffed limit 10;";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    template 'needsignoff.tt', { 'needsignoff' => $sth->fetchall_arrayref };

};

get '/needsdocumenting' => sub {
    my $bugs_dbh = database('bugs');
    my $sql      = "SELECT bugs.bug_id,short_desc FROM bugs
               WHERE bug_status ='Needs documenting' AND product_id = 2 ORDER by lastdiffed limit 10;";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    template 'needsdocumenting.tt', { 'needsdocumenting' => $sth->fetchall_arrayref };
};

true;
