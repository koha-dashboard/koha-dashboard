package KC::Data;

use 5.010001;
use strict;
use warnings;

use LWP::Simple qw($ua get);
use XML::Simple;
use YAML qw/Load/;
use DateTime;
use DBI;
use Encode qw( decode_utf8 );

require Exporter;

our @ISA = qw(Exporter);

our $DEBUG = $ENV{DEBUG} || 0;

our @supported_versions = qw( 23.11 23.05 22.11 22.05 21.11 );

# This allows declaration	use KC::Data ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = (
    'all' => [
        qw(
          get_dates ohloh_activity last5signoffs monthlyrescues yearlyrescues 
          monthlyactivity yearlyactivity monthlydocs yearlydocs health_status get_last_devs get_package_versions get_supported_versions pushed_by_day
          set_now $now $cur_year $cur_month $cur_day $DEBUG
          )
    ]
);

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION = '0.01';

our ( $now, $cur_year, $cur_month, $cur_day, $last_release );

sub set_now {
    my ( $dt ) = shift;
    $cur_year  = sprintf "%04d", $dt->year();
    $cur_month = sprintf "%02d", $dt->month();
    $cur_day   = sprintf "%02d", $dt->day();
    my $may_release = DateTime->new(
        year   => $cur_year,
        month  => 5,
        day    => 22,
        hour   => 0,
        minute => 0,
        second => 1
    );
    if ( DateTime->compare( $dt, $may_release ) == 1) {
        $last_release = $cur_year . "-05-22 00:00:01";
    } else {
        my $year;
        if ( $dt->month() <= 5 ) {
            $year = sprintf "%04d", ($dt->year() - 1);
        } else {
            $year = $cur_year;
        }
        $last_release = $year . "-11-22 00:00:01";
    }
}

sub last5signoffs {
    my $database = shift;
    my $sql =
"SELECT realname,bugs.bug_id,bug_when,short_desc                                            
    FROM bugs_activity,profiles,bugs WHERE bugs.product_id=2 AND bugs_activity.who=profiles.userid
      AND bugs.bug_id=bugs_activity.bug_id AND added='Signed Off' AND bugs.product_id = 2 
        ORDER BY bug_when DESC LIMIT 10";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute or die $sth->errstr;
    my $entries = $sth->fetchall_arrayref;
    return $entries;
}

sub pushed_by_day {
    my $database = shift;
    my $sql = "SELECT added, DATE(bug_when) AS day, count(*) AS count FROM bugs_activity WHERE added LIKE \"Pushed to%\" AND bug_when>=? GROUP BY DATE(bug_when), added ORDER BY day;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($last_release) or die $sth->errstr;
    my $pushed_to_master = { label => 'Pushed to master' };
    my $pushed_to_stable = { label => 'Pushed to stable' };
    my $pushed_to_old = { label => 'Pushed to oldstable' };
    my $pushed_to_oldold = { label => 'Pushed to oldoldstable' };
    while (my $row = $sth->fetchrow_hashref) {
        push @{ $pushed_to_master->{data} },
          { x => $row->{day} . "T00:00:00Z", y => $row->{count} }
          if ( lc($row->{added}) eq 'pushed to master' );
        push @{ $pushed_to_stable->{data} },
          { x => $row->{day} . "T00:00:00Z", y => $row->{count} }
          if ( lc($row->{added}) eq 'pushed to stable' );
        push @{ $pushed_to_old->{data} },
          { x => $row->{day} . "T00:00:00Z", y => $row->{count} }
          if ( lc($row->{added}) eq 'pushed to oldstable' );
        push @{ $pushed_to_oldold->{data} },
          { x => $row->{day} . "T00:00:00Z", y => $row->{count} }
          if ( lc($row->{added}) eq 'pushed to oldoldstable' );
    }
    return {
        datasets => [
            $pushed_to_master, $pushed_to_stable,
            $pushed_to_old,    $pushed_to_oldold
        ]
    };
}

sub monthlyactivity {
    my ( $database, $type ) = @_;
    my $sql =
    "SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND added=? AND YEAR(bug_when) = ? AND MONTH(bug_when) = ? AND who != assigned_to GROUP BY login_name,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($type, $cur_year, $cur_month) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub yearlyactivity {
    my ( $database, $type ) = @_;
    my $sql =
"SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND added=? AND YEAR(bug_when) = ? AND who != assigned_to GROUP BY login_name,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($type, $cur_year) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub monthlyrescues {
    my ($database) = @_;
    my $sql =
"SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND (removed='Failed QA' OR removed=\"Patch doesn't apply\") AND YEAR(bug_when) = ? AND MONTH(bug_when) = ? AND who != assigned_to GROUP BY login_name ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($cur_year, $cur_month) or die $sth->errstr;
    my $rescued = $sth->fetchall_arrayref;
    return $rescued;
}

sub yearlyrescues {
    my ($database) = @_;
    my $sql =
"SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND (removed='Failed QA' OR removed=\"Patch doesn't apply\") AND YEAR(bug_when) = ? AND who != assigned_to GROUP BY login_name ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($cur_year) or die $sth->errstr;
    my $rescued = $sth->fetchall_arrayref;
    return $rescued;
}

sub monthlydocs {
    my ( $database ) = @_;
    my $sql =
    "SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND removed='Needs documenting' AND YEAR(bug_when) = ? AND MONTH(bug_when) = ? AND who != assigned_to GROUP BY login_name,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($cur_year, $cur_month) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub yearlydocs {
    my ( $database ) = @_;
    my $sql =
"SELECT IF(realname = '', 'Anonymous contributor', realname),count(*) FROM bugs_activity,profiles,bugs WHERE bugs_activity.who=profiles.userid AND bugs.bug_id=bugs_activity.bug_id AND removed='Needs documenting' AND YEAR(bug_when) = ? AND who != assigned_to GROUP BY login_name,added ORDER BY count(*) desc;";
    my $sth = $database->prepare($sql) or die $database->errstr;
    $sth->execute($cur_year) or die $sth->errstr;
    my $stats = $sth->fetchall_arrayref;
    return ($stats);
}

sub health_status {
    my ($database) = @_;
    my $sql        = q|
        SELECT bug_id, short_desc, bug_severity, bug_status, creation_ts, lastdiffed, name
        FROM bugs
        LEFT JOIN components ON component_id=id
        WHERE bugs.product_id = 2
            AND bug_severity IN ( 'blocker', 'critical', 'major' )
            AND (
                bug_status IN ('NEW', 'Assigned', 'In Discussion', 'Failed QA', 'Patch doesn\'t apply', 'Needs Signoff', 'Signed Off', 'Passed QA')
            )
        ORDER BY FIELD(bug_severity, 'blocker', 'critical', 'major'), FIELD(bug_status, 'NEW', 'Assigned', 'In Discussion', 'Failed QA', 'Patch doesn\'t apply', 'Needs Signoff', 'Signed Off', 'Passed QA'), lastdiffed desc;
    |;
    my ( @bugs, @current_severity_bunch, @current_status_bunch, $last_severity,
        $last_status, $total );
    my $dt = DateTime->now;
    $cur_year  = sprintf "%04d", $dt->year();
     $cur_month = sprintf "%02d", $dt->month();
         $cur_day   = sprintf "%02d", $dt->day();

    for my $bug (
        @{ $database->selectall_arrayref( $sql, { Slice => {} } )}
      )
    {
        $total->{ $bug->{bug_severity} }++;
        unless (@current_status_bunch) {
            push @current_status_bunch, $bug;
        }
        else {
            if (   $last_status ne $bug->{bug_status}
                or $last_severity ne $bug->{bug_severity} )
            {
                push @current_severity_bunch,
                  { status => $last_status, bugs => [@current_status_bunch] };
                @current_status_bunch = ();
            }
            if ( $last_severity ne $bug->{bug_severity} ) {
                push @bugs,
                  {
                    severity => $last_severity,
                    statuses => [@current_severity_bunch]
                  };
                @current_severity_bunch = ();
            }
            push @current_status_bunch, $bug;
        }
        $last_severity = $bug->{bug_severity};
        $last_status   = $bug->{bug_status};
    }
    push @current_severity_bunch,
      { status => $last_status, bugs => [@current_status_bunch] };
    push @bugs,
      { severity => $last_severity, statuses => [@current_severity_bunch] };

    return { bugs => \@bugs, total => $total };
}

sub get_dates {

    # subroutine to parse a date file and put it into a form suitable for TT
    open( my $FH, '<', 'data/dates.txt' );
    my @dates;
    my $today = DateTime->now->ymd;
    while ( my $line = <$FH> ) {
        my ( $date, $desc ) = split( /\|/, $line );
        my $daterow = {
            'date' => $date,
            'desc' => $desc
        };
        if ( $date gt $today ) {
            push @dates, $daterow;
        }

    }
    close $FH;
    return \@dates;

}

sub ohloh_activity {
    my $url =
"http://www.ohloh.net/projects/koha/analyses/latest/activity_facts.xml?api_key=ad98f4080e21c596b62c9315f6c7a4c8b08af082";

    # get the url from the server
    my $response = get $url or return;

    # parse the XML response
    my $xml = eval { XMLin($response) } or return;

    # was the request a success?
    return
      unless $xml->{status} eq 'success';
    return $xml;
}

sub get_last_devs {
    my $now = shift;
    my $url = "http://git.koha-community.org/gitweb/?p=koha.git;a=blob_plain;f=docs/contributors.yaml;hb=refs/heads/master";

    # get the url from the server
    my $response = get $url or return;

    $response = Encode::decode_utf8($response);

    # parse the YAML response
    my $yaml = eval { Load($response) } or return;

    my @last_5_devs;
    for my $dev (
        reverse # last devs
        sort { $yaml->{$a}->{first_commit} <=> $yaml->{$b}->{first_commit} } # sort by first_commit
        grep { exists $yaml->{$_}->{first_commit} } keys %$yaml # only devs (with first_commit key)
      )
    {
        push @last_5_devs,
          {
            name => $dev,
            date =>
              DateTime->from_epoch( epoch => $yaml->{$dev}->{first_commit} )
              ->ymd('-') # display YYYY-MM-DD
          };
        last if @last_5_devs >= 5;
    }
    return \@last_5_devs;
}

sub get_package_versions {
    my $base_url = 'https://debian.koha-community.org/koha/pool/main/k/koha/current.';
    my $versions = {};
    my $months = {
        Jan => 1,
        Feb => 2,
        Mar => 3,
        Apr => 4,
        May => 5,
        Jun => 6,
        Jul => 7,
        Aug => 8,
        Sep => 9,
        Oct => 10,
        Nov => 11,
        Dec => 12
    };
    for my $version ( @supported_versions ) {
        my $response = get $base_url . $version || next;
        my @lines = split "\n", $response;
        my $first_line = $lines[0];
        if ( $first_line =~ m{koha \((.*)\)} ) {
            my $full_version = $1;
            my $fifth_line = $lines[4];
            my $date;
            if ( $fifth_line =~ m{>\s\s(.*)} ) {
                # Sun, 27 Nov 2022 23:34:19 +1300
                my @date = split(/,| /, $1);
                $date = DateTime->new(
                    day   => $date[2],
                    month => $months->{ $date[3] },
                    year  => $date[4]
                )->ymd;
            }
            $versions->{$version} = { full_version => $full_version, date => $date };
        }
    }
    return $versions;
}

sub get_supported_versions {
    return \@supported_versions;
}

1;

__END__
